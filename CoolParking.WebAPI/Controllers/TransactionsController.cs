﻿using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.ModelsJSON;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;

        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }

        /// <summary>
        /// Retrieves last transactions
        /// </summary>
        /// <response code="200">Success</response>
        // GET: api/<TransactionsController>/capacity
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("last")]
        public ActionResult<List<TransactionInfoSchema>> GetLastParkingTransactions() => 
            Ok(_parkingService.GetLastParkingTransactions());

        /// <summary>
        /// Retrieves transactions from log file
        /// </summary>
        /// <response code="200">Success</response>
        ///<response code="404">Log file isn't exists yet</response>
        // GET: api/<TransactionsController>/all
        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<string> GetAllTransaction()
        {
            try
            {
                var logtext = _parkingService.ReadFromLog();
                return Ok(logtext);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        /// <summary>
        /// TopUp a vehicle by unique id with specified sum
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///        "Id": "AA-0000-AA",
        ///        "Sum": 100
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Request is handled successfully</response>
        /// <response code="400">Body is invalid</response>
        /// <response code="404">Vehicle not found</response>
        // GET api/<VehiclesController>/vehicleId
        // PUT: api/<TransactionsController>/topUpVehicle
        [HttpPut("topUpVehicle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<VehicleSchema> PutTopUpVehicle([FromBody] TopUpVehicleSchema topUpVehicleSchema)
        {
            if (topUpVehicleSchema == null)
                return BadRequest();

            try
            {
                _parkingService.TopUpVehicle(topUpVehicleSchema.Id, topUpVehicleSchema.Sum);
                return Ok(_parkingService.GetVehicles().FirstOrDefault(v => v.Id == topUpVehicleSchema.Id));
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
