﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;

        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        /// <summary>
        /// Retrieves capacity of the parking
        /// </summary>
        /// <response code="200">Success</response>
        // GET: api/<ParkingController>/capacity
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("capacity")]
        public ActionResult<int> GetCapacity() => Ok(_parkingService.GetCapacity());

        /// <summary>
        /// Retrieves balance of the parking
        /// </summary>
        /// <response code="200">Success</response>
        // GET api/<ParkingController>/balance
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("balance")]
        public ActionResult<decimal> GetBalance() => Ok(_parkingService.GetBalance());

        /// <summary>
        /// Retrieves free places of the parking
        /// </summary>
        /// <response code="200">Success</response>
        // GET api/<ParkingController>/freePlaces
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("freePlaces")]
        public ActionResult<int> GetFreePlaces() => Ok(_parkingService.GetFreePlaces());

    }
}
