﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using CoolParking.WebAPI.ModelsJSON;
using System;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;
        private string _pattern;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
            _pattern = Settings.Pattern;
        }

        /// <summary>
        /// Retrieves all vehicles on the parking
        /// </summary>
        /// <response code="200">Success</response>
        // GET: api/<VehiclesController>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<VehicleSchema>> Get() => Ok(_parkingService.GetVehicles());


        /// <summary>
        /// Retrieves a vehicle by unique id
        /// </summary>
        /// <param name="id" example="AA-0000-AA">The vehicle id</param>
        /// <response code="200">Request is handled successfully</response>
        /// <response code="400">Id is invalid</response>
        /// <response code="404">Vehicle not found</response>
        // GET api/<VehiclesController>/vehicleId
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<VehicleSchema> Get(string id)
        {
            if (!Regex.IsMatch(id, _pattern))
                return BadRequest("Wrong Id Format");

            var vehicles = _parkingService.GetVehicles();
            var vehicle = vehicles.FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
                return NotFound($"Vehicle with Plate Number {id} doesn't exists");
            else
            {
                return Ok(new VehicleSchema
                {
                    Id = vehicle.Id,
                    VehicleType = (int)vehicle.VehicleType,
                    Balance = vehicle.Balance
                });
            }

        }

        /// <summary>
        /// Post a vehicle
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///        "Id": "AA-0000-AA",
        ///        "VehicleType": 0,
        ///        "Balance": 100
        ///     }
        ///     
        /// </remarks>
        /// <response code="201">Vehicle created</response>
        /// <response code="400">Vehicle has missing/invalid values</response>
        // POST api/<VehiclesController>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<VehicleSchema> Post([FromBody] VehicleSchema vehicleJson)
        {
            if(!Enum.IsDefined(typeof(VehicleType),vehicleJson.VehicleType))
                return BadRequest("Wrong vehicle type");

            if (vehicleJson == null)
                return BadRequest();

            try
            {
                _parkingService.AddVehicle(new Vehicle(vehicleJson.Id,
                    (VehicleType)vehicleJson.VehicleType, vehicleJson.Balance));

                return CreatedAtAction("POST", vehicleJson);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete a vehicle by unique id
        /// </summary>
        /// /// <param name="id" example="AA-0000-AA">The vehicle id</param>
        /// <response code="204">Request is handled successfully</response>
        /// <response code="400">Id is invalid </response>
        /// <response code="404">Vehicle not found</response> 
        // DELETE api/<VehiclesController>/vehicleId
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Delete([FromRoute] string id)
        {
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            
        }
    }
}
