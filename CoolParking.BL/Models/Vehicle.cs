﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string _id;
        private decimal _balance;
        private static string _pattern = Settings.Pattern;
        public VehicleType VehicleType { get; }

        public static string Pattern { get => _pattern; }

        public string Id
        {
            get => _id;
            set
            {
                if (Regex.IsMatch(value, _pattern))
                    _id = value;
                else
                    throw new ArgumentException("Wrong Id format");
            }
        }

        public decimal Balance
        {
            get => _balance;
            set
            {
                if (value > 0)
                    _balance = value;
                else
                    throw new ArgumentException("Balance can't be less then 1");
            }
        }

        public Vehicle()
        {

        }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = vehicleType;
            Balance = balance;
        }

        public void Withdraw(decimal sum)
        {
            if (sum > 0)
                _balance -= sum;
            else
                throw new ArgumentException();
        }

        public void TopUp(decimal sum)
        {
            if (sum > 0)
                _balance += sum;
            else
                throw new ArgumentException("Sum can't be less then 1");
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            StringBuilder randomId = new StringBuilder(_pattern.Length);

            //[A-Z]{2}
            randomId.Append(((char)random.Next('A', 'Z')).ToString()
                          + ((char)random.Next('A', 'Z')).ToString());
            //-
            randomId.Append('-');
            //[0-9]{4}
            randomId.Append(random.Next(0, 10).ToString() + random.Next(0, 10).ToString()
                          + random.Next(0, 10).ToString() + random.Next(0, 10).ToString());
            //-
            randomId.Append('-');
            //[A-Z]{2}
            randomId.Append(((char)random.Next('A', 'Z')).ToString()
                          + ((char)random.Next('A', 'Z')).ToString());

            return randomId.ToString();
        }

        public override string ToString()
        {
            return $" Id: {_id} | VehicleType: {VehicleType} | Balance: {Balance}";
        }

    }
}