﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    // (Singleton)
    class Parking 
    {
        // Private field that stores parking instance 
        private static Parking _instance;
        // Private field that stores parking capacity 
        public int Capacity;
        // Field that stores parking balance
        public decimal Balance { get; set; }
        // Field that stores List of vehicles on parking
        public List<Vehicle> Vehicles;
        private Parking()
        {
            Capacity = Settings.ParkingCapacity;
            Balance = Settings.ParkingInitialBalance;
            Vehicles = new List<Vehicle>(Settings.ParkingCapacity);
        }

        public void TopUp(decimal sum)
        {
            if (sum > 0)
                Balance += sum;
            else
                throw new ArgumentException("TopUp sum can't be less then 1");
        }

        public static Parking GetInstance()
        {
            if (_instance == null)
                _instance = new Parking();
            return _instance;
        }
    }

}