﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;

        public double Interval
        {
            get => _timer.Interval;
            set
            {
                if (value > 0)
                    _timer.Interval = value;
                else
                    throw new ArgumentException();
            }
        }

        public event ElapsedEventHandler Elapsed
        {
            add => _timer.Elapsed += value;
            remove => _timer.Elapsed -= value;
        }


        public TimerService(double interval, ElapsedEventHandler eventHandler)
        {
            _timer = new Timer();
            Interval = interval;

            if (eventHandler != null)
                Elapsed += eventHandler;
            else
                throw new ArgumentException();
        }

        public void Dispose() => _timer.Dispose();
        public void Start() => _timer.Start();
        public void Stop() => _timer.Stop();
    }
}