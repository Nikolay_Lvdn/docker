﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private List<TransactionInfo> _currentTransactionInfo;


        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            _parking = Parking.GetInstance();
            this._withdrawTimer = _withdrawTimer;
            _withdrawTimer.Elapsed += Withdraw;
            this._logTimer = _logTimer;
            _logTimer.Elapsed += LogTransactions;
            this._logService = _logService;
            _currentTransactionInfo = new List<TransactionInfo>();

            this._withdrawTimer.Start();
            this._logTimer.Start();
        }

        public ParkingService()
        {
            _parking = Parking.GetInstance();
            _withdrawTimer = new TimerService(Settings.PaymentIntervalInSec * 1000, Withdraw);
            _logTimer = new TimerService(Settings.LogSaveIntervalInSec * 1000, LogTransactions);
            _logService = new LogService(Settings.LogFilePath);
            _currentTransactionInfo = new List<TransactionInfo>();

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        public decimal GetBalance() => _parking.Balance;
        public int GetCapacity() => _parking.Capacity;
        public int GetFreePlaces() => (GetCapacity() - _parking.Vehicles.Count);
        public TransactionInfo[] GetLastParkingTransactions() => _currentTransactionInfo?.ToArray();
        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = GetVehicleById(vehicleId);
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("Balance is negative, you can't remove this vehicle");
            _parking.Vehicles.Remove(vehicle);
        }


        public void Dispose()
        {
            LogTransactions(this, null);

            _parking.Vehicles.Clear();
            _parking.Balance = 0;
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _currentTransactionInfo.Clear();
        }

        public string ReadFromLog() => _logService.Read();

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new InvalidOperationException($"There is no place for new vehicle");

            foreach (var v in _parking.Vehicles)
                if (vehicle.Id == v.Id) throw new ArgumentException($"Vehicle with id {vehicle.Id} is already exists");

            _parking.Vehicles.Add(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum) => GetVehicleById(vehicleId).TopUp(sum);

        private Vehicle GetVehicleById(string vehicleId)
        {
            if (!Regex.IsMatch(vehicleId, Vehicle.Pattern))
                throw new ArgumentException($"Wrong Id Format");

            Vehicle vehicle = _parking.Vehicles.Find(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException($"Vehicle with id {vehicleId} doesn't exist");
            else
                return vehicle;
        }

        private void Withdraw(object sender, ElapsedEventArgs e)
        {
            foreach (var v in _parking.Vehicles)
            {
                var paymentAmount = CountPaymentAmount(v);
                v.Withdraw(paymentAmount);
                _currentTransactionInfo.Add(new TransactionInfo(paymentAmount, DateTime.Now, v.Id));
                _parking.Balance += paymentAmount;
            }
        }

        private decimal CountPaymentAmount(Vehicle vehicle)
        {
            Settings.Tariffs.TryGetValue(vehicle.VehicleType, out decimal withdrawAmount);
            if (vehicle.Balance >= withdrawAmount)
                return withdrawAmount;
            else if (vehicle.Balance < withdrawAmount && vehicle.Balance >= 0)
                return (withdrawAmount - vehicle.Balance) * Settings.FineRatio + vehicle.Balance;
            else
                return withdrawAmount * Settings.FineRatio;
        }

        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            string response = null;
            // To prevent exception when _currentTransactionInfo modified while we write it to log
            var ct = _currentTransactionInfo.ToArray();
            _currentTransactionInfo.Clear();
            foreach (var t in ct)
            {
                response += JsonSerializer.Serialize(t) + "\n";
            }

            if (response == null)
                response = "No transaction in this interval\n";

            _logService.Write(response);
        }

    }
}