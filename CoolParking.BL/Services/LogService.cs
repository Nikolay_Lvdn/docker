﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        // Private field that stores path lo log file
        private string _logPath;

        public string LogPath
        {
            get => _logPath;
            private set
            {
                if (!String.IsNullOrEmpty(value))
                    _logPath = value;
                else
                    throw new ArgumentNullException();
            }
        }

        public LogService(string LogPath)
        {
            this.LogPath = LogPath;

            if (File.Exists(_logPath))
                File.Delete(_logPath);
        }

        public string Read()
        {
            string logText = null;

            if (!File.Exists(_logPath))
                throw new InvalidOperationException("File doesn't exists");
            using (StreamReader streamReader = new StreamReader(_logPath))
            {
                if (streamReader == null)
                    throw new InvalidOperationException();
                logText = streamReader.ReadToEnd();
                streamReader.Close();
            }

            return logText;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = new StreamWriter(_logPath, true))
            {
                if (streamWriter == null)
                    throw new InvalidOperationException();
                streamWriter.WriteLine(logInfo);
                streamWriter.Close();
            }
        }


    }
}